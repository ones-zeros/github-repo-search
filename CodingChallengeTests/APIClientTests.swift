//
//  APIClientTests.swift
//  CodingChallengeTests

import XCTest
import Combine
import Hippolyte

@testable import CodingChallenge

class APIClientTests: XCTestCase {
    
    var apiClient: APIClient!
    var disposables = Set<AnyCancellable>()
    var request:GitHubRequest!
    var url:URL!
    
    override func setUp() {
        apiClient = APIClient()
        request = GitHubRequest(name: "swift")
        url = request.request(with: URL(string: "https://api.github.com/")!).url
        
    }

    override func tearDown() {
        Hippolyte.shared.stop()
        super.tearDown()
    }

    func testSendSuccess() {
        var stub = StubRequest(method: .GET, url: url)
        var response = StubResponse.Builder()
            .stubResponse(withStatusCode: 200)
            .build()

        let mockedBody = """
{
        "total_count": 2,
        "incomplete_results": false,
        "items": [
            {
                "id": 11111,
                "name": "Name1",
                "full_name": "Name1/Name1",
                "private": false
            },
            {
                "id": 22222,
                "name": "Name2",
                "full_name": "Name2/Name2",
                "private": true
            }
        ]
}
"""
            .data(using: .utf8)!

        response.body = mockedBody
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let expectation = self.expectation(description: "Mock network call success")
        let publisher:AnyPublisher<GitHubSearchResults, APIError> = apiClient.send(apiRequest: request)
        publisher.sink(receiveCompletion: { [weak self] value in
            guard let _ = self else { return }
            switch value {
            case .failure:
                XCTFail("Sink failed")
            case .finished:
                break
            }
            }
            , receiveValue: { [weak self] result in
                guard let _ = self else {
                    return
                }
                XCTAssertEqual(result.items.count, 2)
                XCTAssertEqual(result.totalCount, 2)
                XCTAssertFalse(result.incompleteResults)
                expectation.fulfill()
            })
            .store(in: &disposables)
        
        wait(for: [expectation], timeout: 1)
    }

    func testSendNetworkError() {
        var stub = StubRequest(method: .GET, url: url!)
        let response = StubResponse.Builder()
            .stubResponse(withStatusCode: 500)
            .build()
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let expectation = self.expectation(description: "Mock network call error")
        let publisher:AnyPublisher<GitHubSearchResults, APIError> = apiClient.send(apiRequest: request)
        publisher.sink(receiveCompletion: { [weak self] value in
            guard let _ = self else { return }
            switch value {
            case .failure(let error):
                switch error {
                case .network:
                        expectation.fulfill()
                    default:
                        XCTFail()
                }
                break
            case .finished:
                XCTFail("Should not succeed")
                break
            }
            }
            , receiveValue: { [weak self] result in
                guard let _ = self else {
                    return
                }
                XCTFail("Should not get a result: \(result)")
                
        })
            .store(in: &disposables)
        
        wait(for: [expectation], timeout: 1)
    }
    
    func testSend403Error() {
        var stub = StubRequest(method: .GET, url: url!)
        let response = StubResponse.Builder()
            .stubResponse(withStatusCode: 403)
            .build()
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let expectation = self.expectation(description: "Mock network call error")
        let publisher:AnyPublisher<GitHubSearchResults, APIError> = apiClient.send(apiRequest: request)
        publisher.sink(receiveCompletion: { [weak self] value in
            guard let _ = self else { return }
            switch value {
            case .failure(let error):
                switch error {
                    case .http403:
                        expectation.fulfill()
                    default:
                        XCTFail()
                }
                break
            case .finished:
                XCTFail("Should not succeed")
                break
            }
            }
            , receiveValue: { [weak self] result in
                guard let _ = self else {
                    return
                }
                XCTFail("Should not get a result: \(result)")
                
        })
            .store(in: &disposables)
        
        wait(for: [expectation], timeout: 1)
    }
    
    func testSend422Error() {
           var stub = StubRequest(method: .GET, url: url!)
           let response = StubResponse.Builder()
               .stubResponse(withStatusCode: 422)
               .build()
           stub.response = response
           Hippolyte.shared.add(stubbedRequest: stub)
           Hippolyte.shared.start()
           
           let expectation = self.expectation(description: "Mock network call error")
           let publisher:AnyPublisher<GitHubSearchResults, APIError> = apiClient.send(apiRequest: request)
           publisher.sink(receiveCompletion: { [weak self] value in
               guard let _ = self else { return }
               switch value {
               case .failure(let error):
                   switch error {
                       case .http422:
                           expectation.fulfill()
                       default:
                           XCTFail()
                   }
                   break
               case .finished:
                   XCTFail("Should not succeed")
                   break
               }
               }
               , receiveValue: { [weak self] result in
                   guard let _ = self else {
                       return
                   }
                   XCTFail("Should not get a result: \(result)")
                   
           })
               .store(in: &disposables)
           
           wait(for: [expectation], timeout: 1)
       }
    
    func testSendParseError() {
        let mockedBadBody = """
        {
                "error": "something happened"
        }
        """.data(using: .utf8)!

        var stub = StubRequest(method: .GET, url: url!)
        var response = StubResponse.Builder()
            .stubResponse(withStatusCode: 200)
            .build()
        response.body = mockedBadBody
        stub.response = response
        Hippolyte.shared.add(stubbedRequest: stub)
        Hippolyte.shared.start()
        
        let expectation = self.expectation(description: "Mock parsing error")
        let publisher:AnyPublisher<GitHubSearchResults, APIError> = apiClient.send(apiRequest: request)
        publisher.sink(receiveCompletion: { [weak self] value in
            guard let _ = self else { return }
            switch value {
            case .failure(let error):
                switch error {
                case .parsing:
                    expectation.fulfill()
                default:
                    XCTFail()
                }
                break
            case .finished:
                XCTFail("Should not succeed")
                break
            }
            }
            , receiveValue: { [weak self] result in
                guard let _ = self else {
                    return
                }
                XCTFail("Should not get a result: \(result)")
                
        })
            .store(in: &disposables)
        
        wait(for: [expectation], timeout: 1)
    }
}
