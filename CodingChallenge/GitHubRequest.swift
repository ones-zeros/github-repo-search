//
//  GitHubRequest.swift
//  CodingChallenge
//

import Foundation

class GitHubRequest: APIRequest {
    var method = HttpMethod.get
    var path = "search/repositories"
    var parameters = [String: String]()
    
    init(name: String) {
        parameters["q"] = name
        parameters["sort"] = "stars"
        parameters["order"] = "desc"
    }
}

