//
//  RepoViewModel.swift
//  CodingChallenge
//

import Foundation

struct RepoViewModel: Equatable, Identifiable {

    private let repo: GitHubRepo
    
    var id: Int {
        repo.id
    }
    
    var title: String {
        repo.name
    }
    
    init(repo: GitHubRepo) {
      self.repo = repo
    }

    var url: String {
        repo.contributorsURL
    }
}
