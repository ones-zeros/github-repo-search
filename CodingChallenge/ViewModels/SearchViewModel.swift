//
//  SearchViewModel.swift
//  CodingChallenge
//

import Foundation
import Combine

class SearchViewModel: ObservableObject, Identifiable {
    
    @Published var searchText: String = ""
    @Published var repoList: [RepoViewModel] = []  // Search API provides 1,000 results for each search.
    @Published var showError = false
    
    static let pageSize = 100
    static let preloadOffset = pageSize / 2
    static let githubLimit = 1000
    static let minimumCharacterCount = 2
    static let delayInSeconds = 3.0
    
    var pageNumber: Int = 1
    var totalCount = -1
    var errorMessage = ""
    
    private let apiClient = APIClient()
    private var disposables = Set<AnyCancellable>()
    private var scheduler: DispatchQueue = DispatchQueue(label: "SearchViewModelQueue", attributes: .concurrent)
    private var currentListSearch = ""
    private var cancellableSubscriber: AnyCancellable?
    
    init() {
        $searchText
            .dropFirst(1)
            .debounce(for: .seconds(SearchViewModel.delayInSeconds), scheduler: scheduler)
            .removeDuplicates(by: { (s1, s2) in
                return s1.lowercased() == s2.lowercased()
            })
            .sink(receiveValue: startFetchData(forName:))
            .store(in: &disposables)
    }
    
    func startFetchData(forName name: String) {
        if !isValidSearch(name) {
            return
        }
        pageNumber = 1
        let request = GitHubRequest(name: name)
        request.parameters["page"] = String(pageNumber)
        request.parameters["per_page"] = String(SearchViewModel.pageSize)
        fetchDataForRequest(request: request, isNewSearch: true)
    }
    
    func prefetchIfNeeded(currentItem: RepoViewModel) {
        // If there is a new valid search issued, current search is stale, don't page
        if isValidSearch(searchText)
            && currentListSearch != searchText.trimmingCharacters(in: [" "]) {
            return
        }
        // If the number we are 1/2 a page away from the end, prefetch next page UNLESS
        // we have reached the totalCount of hit or the hard limit imposed by Github API (1000)
        if repoList.index(of: currentItem) == repoList.count - SearchViewModel.preloadOffset
            && repoList.count < min(totalCount, SearchViewModel.githubLimit) {
            let page = repoList.count / SearchViewModel.pageSize + 1
            if page > self.pageNumber {
                pageNumber = page
                scheduler.async {
                    let request = GitHubRequest(name: self.searchText)
                    request.parameters["page"] = String(page)
                    request.parameters["per_page"] = String(SearchViewModel.pageSize)
                    self.fetchDataForRequest(request: request, isNewSearch: false)
                }
            }
        }
    }
    
    func fetchDataForRequest(request: GitHubRequest, isNewSearch: Bool) {
        DispatchQueue.main.async {
            self.errorMessage = ""
            self.showError = false
        }
        // Cancel any existing previous request
        if let subs = cancellableSubscriber {
            subs.cancel()
            cancellableSubscriber = nil
        }
  
        let publisher:AnyPublisher<GitHubSearchResults, APIError> = apiClient.send(apiRequest: request)
        let subscriber = publisher.receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { return }
                    switch value {
                    case .failure(let error):
                        if isNewSearch {
                            self.repoList = []
                            self.totalCount = 0
                            self.currentListSearch = self.searchText.trimmingCharacters(in: [" "]) // Keep track of search and it's result
                        }
                        self.errorMessage = error.getErrorMessage()
                        self.showError = true
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] searchResult in
                    guard let self = self else { return }
                    if isNewSearch {
                        self.repoList = searchResult.items.map(RepoViewModel.init)
                        self.currentListSearch = self.searchText.trimmingCharacters(in: [" "]) // Keep track of search
                    } else {
                        var temp = self.repoList
                        temp.append(contentsOf: searchResult.items.map(RepoViewModel.init))
                        self.repoList = temp
                    }
                    self.totalCount = searchResult.totalCount
            })
        
        subscriber.store(in: &disposables) // Keep a reference to publisher until done
        cancellableSubscriber = AnyCancellable(subscriber)
    }
    
    private func isValidSearch(_ search: String) -> Bool {
        let s = search.trimmingCharacters(in: [" "])
        return s.count > SearchViewModel.minimumCharacterCount
    }
}

class URLSessionHandler: NSObject, URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate) {
            completionHandler(.rejectProtectionSpace, nil)
        }
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(.useCredential, credential)
        }
    }
}
