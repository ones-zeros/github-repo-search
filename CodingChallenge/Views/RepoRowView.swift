//
//  RepoRowView.swift
//  CodingChallenge
//

import SwiftUI

struct RepoRowView: View {
    private let viewModel: RepoViewModel
    
    init(viewModel: RepoViewModel) {
      self.viewModel = viewModel
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            NavigationLink(destination: WebView(pageURL: viewModel.url)) {
                HStack(alignment: .top, spacing: 100.0) {
                    Text("\(viewModel.title)")
                        .font(.headline)
                        .foregroundColor(.primary)
                }
            }
        }
    }
}
