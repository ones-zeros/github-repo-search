//
//  SearchInputView.swift
//  CodingChallenge
//

import SwiftUI

struct SearchInputView: View {
    @ObservedObject var viewModel: SearchViewModel
    @State private var editMode: Bool = false
    
    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            // Search view
            HStack {
                HStack {
                    // Magnifier
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(.secondary)
                    // Input text
                    TextField("Search", text: $viewModel.searchText, onEditingChanged: { isEditing in
                        self.editMode = isEditing
                    })
                    // Clear button
                    Button(action: {
                        self.viewModel.searchText = ""
                    }) {
                        Image(systemName: "xmark.circle.fill")
                            .opacity(self.viewModel.searchText == "" ? 0 : 1)
                            .foregroundColor(Color(.systemGray))
                    }
                }
                .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
                .background(Color(.secondarySystemBackground))
                .cornerRadius(10)
                
                if editMode  {
                    Button("Cancel") {
                        UIApplication.shared.endEditing(true)
                        self.viewModel.searchText = ""
                    }
                    .foregroundColor(Color(.systemBlue))
                }
            }
            .padding(.horizontal)
            .padding(.top, editMode ? nil : 0)
            .navigationBarTitle(Text("Github Search"))
            .navigationBarHidden(editMode)
        }
    }
}

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter{$0.isKeyWindow}
            .first?
            .endEditing(force)
    }
}
