//
//  SearchView.swift
//  CodingChallenge
//

import SwiftUI

struct SearchMainView: View {
    @ObservedObject var viewModel: SearchViewModel
    
    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        
        NavigationView {
            VStack {
                // Search input view
                SearchInputView(viewModel: viewModel)
                if viewModel.totalCount > -1 {
                    Text("Total found: \(viewModel.totalCount)")
                    .font(.callout)
                    .foregroundColor(.secondary)
                }
                //Result list
                List {
                    ForEach(viewModel.repoList, id: \.id) { item in
                        RepoRowView(viewModel: item).onAppear {
                            self.viewModel.prefetchIfNeeded(currentItem: item)
                        }
                    }
                }
                .resignKeyboardOnDragGesture()
                .animation(.default)
            }
            .alert(isPresented: $viewModel.showError) {
                Alert(title: Text("Alert"), message: Text(viewModel.errorMessage), dismissButton:.default(Text("OK")))
            }
        }
    }
}

struct ResignKeyboardOnDragGesture: ViewModifier {
    var gesture = DragGesture().onChanged { _ in
        UIApplication.shared.endEditing(true)
    }
    func body(content: Content) -> some View {
        content.gesture(gesture)
    }
}

extension View {
    func resignKeyboardOnDragGesture() -> some View {
        return modifier(ResignKeyboardOnDragGesture())
    }
}

