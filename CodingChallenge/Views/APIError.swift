//
//  APIError.swift
//  CodingChallenge
//

import Foundation

enum APIError: Error {
    case parsing(description: String)
    case network(description: String)
    case http422(description: String)
    case http403(description: String)
    
    func getErrorMessage() -> String {
        switch self {
        case .http403:
            return "Permission denied: you may be throttled"
        case .http422:
            return "You have reached the size limit"
        case .parsing:
            return "Data has wrong format"
        case .network:
            return "Unknown network error"
        }
    }
}
