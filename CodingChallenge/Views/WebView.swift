//
//  WebView.swift
//  CodingChallenge
//

import Foundation
import SwiftUI
import WebKit


// UIViewRepresentable, wraps UIKit views for use with SwiftUI
struct WebView: UIViewRepresentable {
    var pageURL: String     // Page to load

    func makeUIView(context: Context) -> WKWebView {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.contentMode = .scaleAspectFit
        return webView
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        guard let url = URL(string: pageURL) else {
            return
        }
        uiView.load(URLRequest(url: url))     // Send the command to WKWebView to load our page
    }
}
