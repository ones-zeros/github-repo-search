//
//  API.swift
//  CodingChallenge
//

import Foundation
import Combine

public enum HttpMethod: String {
    case get, post, put, delete
}

protocol APIRequest {
    var method: HttpMethod { get }
    var path: String { get }
    var parameters: [String : String] { get }
}

extension APIRequest {
    func request(with baseURL: URL) -> URLRequest {
        guard var components = URLComponents(url: baseURL.appendingPathComponent(path),
                                             resolvingAgainstBaseURL: false) else {
            fatalError("Unable to create URL components")
        }
        
        components.queryItems = parameters.map {
            URLQueryItem(name: String($0), value: String($1))
        }
        
        guard let url = components.url else {
            fatalError("Could not get url")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
}

final class APIClient {
    private let baseURL = URL(string: "https://api.github.com/")!
    private var disposables = Set<AnyCancellable>()

    private var session: URLSession {
        return URLSession(configuration: URLSessionConfiguration.default, delegate: URLSessionHandler(), delegateQueue: operationSerialQueue)
    }

    private var operationSerialQueue: OperationQueue {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }

    
    func send<T: Codable>(apiRequest: APIRequest) -> AnyPublisher<T, APIError> {
        return session.dataTaskPublisher(for: apiRequest.request(with: baseURL))
        .tryMap {(data, response) -> Data in
            let response = response as! HTTPURLResponse
            
            if response.statusCode == 200 {
                return data
            }
            let localizedStatusCodeDescription =
                HTTPURLResponse.localizedString(forStatusCode: response.statusCode)
            throw NSError(domain: "", code: response.statusCode, userInfo: [NSLocalizedDescriptionKey: localizedStatusCodeDescription])
            
        }
        .mapError { error in
            let err = error as NSError
            switch err.code {
            case 422:
                return APIError.http422(description: error.localizedDescription)
            case 403:
                return APIError.http403(description: error.localizedDescription)
            default:
                return APIError.network(description: error.localizedDescription)
            }
        }
        .flatMap(maxPublishers: .max(1)) { data in
            decodeToObject(data)
        }
        .eraseToAnyPublisher()
    }
}
