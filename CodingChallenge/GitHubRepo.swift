//
//  GitHubRepo.swift
//  CodingChallenge
//

import Foundation
import Combine

struct GitHubSearchResults: Codable {
    let totalCount: Int
    let incompleteResults: Bool
    let items: [GitHubRepo]
    
    private enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case items
    }
}

struct GitHubRepo: Codable, Equatable {
    let id: Int
    let name: String
    let fullName: String
    let isPrivate: Bool
    let contributorsURL: String
    let description: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case isPrivate = "private"
        case contributorsURL = "contributors_url"
        case description
    }
}

func decodeToObject<T: Decodable>(_ data: Data) -> AnyPublisher<T, APIError> {
  let decoder = JSONDecoder()
  decoder.dateDecodingStrategy = .secondsSince1970

  return Just(data)
    .decode(type: T.self, decoder: decoder)
    .mapError { error in
        .parsing(description: error.localizedDescription)
    }
    .eraseToAnyPublisher()
}
